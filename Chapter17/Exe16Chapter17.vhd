--------------------------------------------------------------------------------
-- Projetc:BCD To 7 Segment decoder
-- File: Exe16Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File Creates a decoder from BCD bunary representtion to a
--              7 Segment.
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity BCD_To_7Seg is
	port (
		A,B,C,D : in  std_logic;
		X       : out std_logic_vector(7 downto 1)
	);
End Entity BCD_To_7Seg;

Architecture Behavioral of BCD_To_7Seg is
	signal sel : std_logic_vector(3 downto 0);
Begin
	sel <= A&B&C&D;

	Process(sel)
	Begin
		Case sel is
			when "0000" => -- Number 0 
				X <= "0111111";
			when "0001" => -- Number 1
				X <= "0000110";
			when "0010" => -- Number 2
				X <= "1101101";
			when "0011" => -- Number 3 
				X <= "1111001";
			when "0100" => -- Number 4
				X <= "0110011";
			when "0101" => -- Number 5
				X <= "0110000";
			when "0110" => -- Number 6 
				X <= "1011111";
			when "0111" => -- Number 7
				X <= "1110000";
			when "1000" => -- Number 8
				X <= "1111111";
			when "1001" => -- Number 9
				X <= "1111011";
			when others => -- Null
				X <= "0000000";
		End Case;
	End Process;
End Architecture Behavioral;