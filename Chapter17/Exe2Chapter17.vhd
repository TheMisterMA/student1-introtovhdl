--------------------------------------------------------------------------------
-- Projetc: Right Shift Register
-- File: Exe2Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File create Right Shift Register Entity and defines it's
--              Architecture.
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity RightSR is
	port (
		Clk, Ld, Rs, Clr, Lin : in  std_logic;
		D                     : in  std_logic_vector(3 downto 0);
		Q                     : out std_logic_vector(3 downto 0)
	);
End Entity RightSR;

Architecture Behavioural of RightSR is
	signal Qcur : std_logic_vector(3 downto 0);
Begin
	Q <= Qcur;

	Process(Clk)
	Begin
		if rising_edge(Clk) then

			-- Clears The Register, 
			-- overrides all the other instructions.
			if CLR = '1' then Qcur <= "0000"; 

			-- Loads The value in D into the Current Q,
			-- overrides only Rs.
			elsif Ld = '1' then Qcur <= D;

			-- Right Shifts the register.
			elsif Rs = '1' then Qcur <= Lin&Qcur(3 downto 1);
			End if;
		End if;
	End Process;
End Architecture Behavioural;