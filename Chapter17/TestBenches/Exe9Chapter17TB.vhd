--------------------------------------------------------------------------------
-- Projetc: Set-Reset Flip
-- File: Exe9Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File tests a Set-Reset Filp Flop entity
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use IEEE.numeric_std.all;

Entity SR_FF_TB is
End Entity SR_FF_TB;

Architecture Test_SR_FF of SR_FF_TB is
	Component SR_FF is
		port (
			S, R, Clk : in  std_logic;
			Q, Not_Q  : out std_logic
		);
	End Component SR_FF;
	signal S, R, Clk, Q, Not_Q : std_logic;
	signal Clk_period          : time := 5 ns;
	signal SR: std_logic_vector(1 downto 0);
Begin
	Test_fliplop : SR_FF
		port map (
			S, R, Clk, Q, Not_Q
		);

	Proc_Clock : Process
	Begin
		Clk <= '1';
		wait for Clk_period;
		Clk <= '0';
		wait for Clk_period;
	End Process;


	Stimulate : Process
	Begin
		S <= '0'; 
		R <= '0';
		SR <= S&R;
		wait for 10 ns;
		for iterator in 2 downto 0 Loop
			SR <= SR+'1';
			S <= SR(1);
			R <= SR(0);
			wait for 10 ns;

			S <= '0'; 
			R <= '0';
			wait for 10 ns;
		End Loop ;
	End Process;
End Architecture Test_SR_FF;