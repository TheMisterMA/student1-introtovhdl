--------------------------------------------------------------------------------
-- Projetc: 2 to 1 MUX with a Process Test Bench
-- File: Exe4Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File Tests a 2 to 1 MUX  
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity MUXs_TB is
End Entity MUXs_TB;

Architecture Test_Of_Mux of MUXs_TB is
	Component Some_MUX is
		port (
			A,B,C,D : in  std_logic;
			Z       : out std_logic
		);
	End Component Some_MUX;
	signal A,B,C,D,Z : std_logic;
Begin
	Tested_Some_MUX : Some_MUX
		port map (
			A,B,C,D,Z
		);

	Stimulate : Process
	Begin
		A <= '0'; B <= '0'; C <= '1'; D <= '1';
		wait for 10 ns;

		A <= '1'; B <= '0';
		wait for 10 ns;

		A <= '0'; B <= '1';
		wait for 10 ns;

		A <= '1'; B <= '1';
		wait for 10 ns;
	End Process;
End Architecture Test_Of_Mux;