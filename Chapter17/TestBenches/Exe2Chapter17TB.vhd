--------------------------------------------------------------------------------
-- Projetc: Right Shift Register Test Bench
-- File: Exe2Chapter17TB.vhd
--------------------------------------------------------------------------------
-- Description: This File tests the work of the Right Shift Register
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity RightSR_TB is
End Entity RightSR_TB;

Architecture RSR_TestBench of RightSR_TB is
	Component RightSR is
		port (
			Clk, Ld, Rs, Clr, Lin : in  std_logic;
			D                     : in  std_logic_vector(3 downto 0);
			Q                     : out std_logic_vector(3 downto 0)
		);
	End Component RightSR;

	signal Clk, Ld, Rs, Clr, Lin : std_logic;
	signal D, Q                  : std_logic_vector(3 downto 0);
	constant Clk_period          : time := 10 ns;
Begin
		RightShiftRegister : RightSR port map (
			Clk, Ld, Rs, Clr, Lin, D, Q
		);


	Proc_Clock : Process
	Begin
		Clk <= '1';
		wait for (Clk_period/2);
		Clk <= '0';
		wait for (Clk_period/2);
	End Process;

	Stimulate : Process
	Begin
		-- Load a new value to the register.
		Ld <= '1';
		D <= "1001";

		-- Resets all the other signals.
		Rs <= '0'; 
		Lin <= '1'; 
		Clr <= '0'; 

		--Waits a little bit to make sure everything had updated.
		wait for 10 ns;

		-- Resets the load, and sets the Right Shift signal.
		-- and wait for 100 ns to let it shift.
		Ld <= '0'; 
		Rs <= '1';
		wait for 100 ns;

		-- Clears the Register and gives it time to update.
		-- and resets the Right Shift
		Rs <= '0';
		Clr <= '1';
		wait for 10 ns;

		-- Resets the Clr and waits another bit, to make sure everything is fine.
		Clr <= '0';
		wait for 50 ns;
	End Process;

End Architecture RSR_TestBench;