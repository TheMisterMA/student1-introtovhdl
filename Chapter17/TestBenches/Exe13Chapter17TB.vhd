--------------------------------------------------------------------------------
-- Projetc: 4 to 2 Proirity Encoder Test Bench
-- File: Exe13Chapter17TB.vhd
--------------------------------------------------------------------------------
-- Description: This File test the 4 to 2 Priotity Encoder 
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Encoder_TB is
End Entity Encoder_TB;

Architecture Test_Pri_Encoder of Encoder_TB is
	Component Encoder_4To2 is
		port (
			Y       : in  std_logic_vector(3 downto 0);
			X, Z, d : out std_logic
		);
	End Component Encoder_4To2;
	signal Y     : std_logic_vector(3 downto 0);
	signal X,Z,d : std_logic;
Begin
	Tested_Encoder : Encoder_4To2
		port map (
			Y,X,Z,
			d
		);

	Stimulate : Process
	Begin
		Y <= "0000";
		wait for 10 ns;

		Y <= "0001";
		wait for 10 ns;

		Y <= "0010";
		wait for 10 ns;

		Y <= "0100";
		wait for 10 ns;

		Y <= "1000";
		wait for 10 ns;
	End Process;
End Architecture Test_Pri_Encoder;