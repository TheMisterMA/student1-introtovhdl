--------------------------------------------------------------------------------
-- Projetc:BCD To 7 Segment decoder Test Bench
-- File: Exe16Chapter17TB.vhd
--------------------------------------------------------------------------------
-- Description: This File tests a decoder from BCD bunary representtion to a
--              7 Segment.
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use IEEE.numeric_std.all;

Entity BCD_7Seg_TB is
End Entity BCD_7Seg_TB;

Architecture Test_BCD_To_7Seg of BCD_7Seg_TB is
	Component BCD_To_7Seg is
		port (
			A,B,C,D : in  std_logic;
			X       : out std_logic_vector(7 downto 1)
		);
	End Component BCD_To_7Seg;
	signal A : std_logic_vector(3 downto 0):= "0000";
	signal X : std_logic_vector(6 downto 0);
Begin
	Converter_To_Test : BCD_To_7Seg
		port map (
			A(3),A(2),A(1),A(0),X
		);

	Simulate : Process
	Begin
		-- Iterates over 11 nummbers, 10 from acceptable representation
		-- and 1 for the others.
		for iterator in 10 downto 0 Loop
			wait for 20 ns;
			A <= A+'1';
		End Loop;

		A <= "0000"
		wait for 20 ns;
	End Process;
End Architecture Test_BCD_To_7Seg;