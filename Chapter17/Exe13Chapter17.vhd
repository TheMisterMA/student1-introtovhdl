--------------------------------------------------------------------------------
-- Projetc:4 to 2 Proirity Encoder
-- File: Exe13Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File Creates an 4 to 2 Priotity Encoder 
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Encoder_4To2 is
	port (
		Y    : in  std_logic_vector(3 downto 0); -- A vector of the terms
		X, Z : out std_logic;                    -- The arguments creatin the terms.
		d    : out std_logic                     -- A flag to diffrentiate between Y = "0000", 
	                                             -- and Y="1000" as both are 0,
	                                             -- but in one of them there is no 1.
	);
End Entity Encoder_4To2;

Architecture Pri_Enc_4to2 of Encoder_4To2 is
Begin
	Process(Y)
	Begin
		if Y(0) = '1' then -- For Y = "XXX1" then X = Z = d = 1
			X <= '1';
			Z <= '1';
			d <= '1';
		elsif Y(1) = '1' then -- For Y = "XX10" then X = d = 1, Z = 0
			X <= '1';
			Z <= '0';
			d <= '1';
		elsif Y(2) = '1' then -- For Y = "X100" then Z = d = 1, X = 0
			X <= '0';
			Z <= '1';
			d <= '1';
		elsif Y(3) = '1' then -- For Y = "1000" then X = Z = 0, d = 1
			X <= '0';
			Z <= '0';
			d <= '1';
		else -- For Y = "0000" then X = Z = d = 0
			X <= '0';
			Z <= '0';
			d <= '0';
		End if;
	End Process;
End Architecture Pri_Enc_4to2;