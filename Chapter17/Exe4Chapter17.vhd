--------------------------------------------------------------------------------
-- Projetc: 2 to 1 MUX with a Process
-- File: Exe4Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File creates a 2 to 1 MUX With 
--              sequantial statments in a Processs
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Some_MUX is
	port (
		A,B,C,D : in  std_logic;
		Z       : out std_logic
	);
End Entity Some_MUX;

Architecture Case_Process of Some_MUX is
	signal sel : std_logic_vector(1 downto 0);
Begin
	sel <= A&B;

	Process(A, B, C, D) -- The MUX Process itself.
	Begin
		Case sel is
			when "00" =>
				Z <= not C or D;
			when "01" =>
				Z <= C;
			when "10" =>
				Z <= not C xor D;
			when "11" =>
				Z <= not D;
			when others => Null;
		End Case;
	End Process;
End Architecture Case_Process;