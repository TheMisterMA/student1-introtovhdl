--------------------------------------------------------------------------------
-- Projetc: Set-Reset Flip
-- File: Exe4Chapter17.vhd
--------------------------------------------------------------------------------
-- Description: This File creates a Set-Reset Filp Flop entity
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity SR_FF is
	port (
		S, R, Clk : in  std_logic;
		Q, Not_Q  : out std_logic
	);
End Entity SR_FF;

Architecture Set_Reset_FF of SR_FF is
	signal Qcur : std_logic := '0';
Begin
	Q     <= Qcur;
	Not_Q <= not Qcur;

	Process(Clk)
	Begin
		if CLK'event and CLK = '1' then
			if R = '1' and S = '1' then
				Qcur <= 'X'; -- R=S=1 is undefined state, and will be 'X'
			elsif S = '1' then
				Qcur <= '1'; -- S = 1, R = 0 will set Q as '1'
			elsif R = '1' then
				Qcur <= '0'; -- R = 1. S = 0 will reset Q to '0'
			End if;
		-- For any other combination, Q will stay the same.
		End if;
	End Process;
End Architecture Set_Reset_FF;