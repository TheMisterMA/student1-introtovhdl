library ieee;
use ieee.std_logic_1164.all;

Entity Quad_MUX is
	port (
		A: std_logic;
		X,Y: bit_vector 3 downto 0;
		Z: bit_vector 3 downto 0
	);
End Entity Quad_MUX;

Architecture Four_Bit_MUX of Quad_MUX is
Begin
	Z <= X when A = '1' else Y; 
	-- Chooses X When A is a logical '1' else it chooses Y, that is if is a logical'0'
End Architecture Four_Bit_MUX;