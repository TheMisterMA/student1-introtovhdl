Library IEEE;
use IEEE.std_logic_1164.all;

Entity Avarage_OF_4 is
	port(
		A,B,C,D : in  std_logic_vector(0 to 15);
		avarage : out std_logic_vector(0 to 15)
	);
End Entity Avarage_OF_4;

Architecture behaviour of Avarage_OF_4 is
	-- Because the sum is of 4 16bit numbers, 
	-- the maximum amount coud be a 18bit number.
	signal sum : std_logic_vector(0 to 17);

Begin
	-- Increases the A vector to a 18bi tlong vector.
	-- This operation Adds each element to its match 
	-- in each variable, like an adder.
	sum <= ("00"&A)+B+C+D;

	-- The leftover of the sum is the remainder, if it is 00 or 01
	-- than the number is more close to the number already achieaved.
	-- else it is close to the next number, so it adds the first num 
	-- of the left over duo to represent the number it is more close to.
	avarage <= sum(17 to 2) + sum(1);
End Architecture behaviour;