Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Library work;
use work.PhoneNum.all;

Entity TB_BrokenPhone is
End Entity TB_BrokenPhone;

Architecture TestBrokenPhone of TB_BrokenPhone is
	Component BrokenPhone is
		port(
			But         : in  integer range 0 to 9;
			isInCall    : in  Boolean;
			IncomingNum : in  std_logic_vector(40 downto 1);
			BlowBomb    : out std_logic
		);
	End Component BrokenPhone;

	signal But         : integer range 0 to 9;
	signal isInCall    : Boolean;
	signal IncomingNum : std_logic_vector(40 downto 1);
	signal BlowBomb    : std_logic;
Begin
	Tested_BrokenPhone : BrokenPhone
		port map (
			But, isInCall, IncomingNum, BlowBomb
		);

	Stimulate : Process
	Begin
		But <= 5;
		wait for 5ns;

		But <= 0;
		wait for 5ns;

		But <= 0;
		wait for 5ns;

		But <= 1;
		wait for 5 ns;

		isInCall <= True; 
		IncomingNum <= "0000010101000111011110001001100001010010"; -- Bit stream of 0547789852 BCD form
		wait for 5ns;

		isInCall <= False;
		wait for 5ns;

		isInCall <= True; 
		IncomingNum <= "0000010100000000000000010001000100100010"; -- Bit stream of 0500011122 BCD form
		wait for 5sec;
	End Process Stimulate;
End Architecture TestBrokenPhone;