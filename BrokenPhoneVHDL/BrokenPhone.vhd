Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity BrokenPhone is
	port(
		But         : in  integer range 0 to 9;
		isInCall    : in  Boolean;
		IncomingNum : in  std_logic_vector(40 downto 1); -- Bit Stream of bcd form of the digigts of the phone number
		BlowBomb    : out std_logic
	);
End Entity BrokenPhone;

Architecture Behaviuoral of BrokenPhone is
	signal State, NextState : integer range 0 to 4 := 0;
	signal DelayToBlow      : time                 := 0sec;
	constant BlowPhoneNum   : std_logic_vector(40 downto 1)
		:= "0000010100000000000000010001000100100010"; -- Bit Stream of bcd form of 0500011122
Begin
	State <= NextState;

	Preparing_And_Blowing : Process(State, But, isInCall)
	Begin
		Case State is
			When 0 =>
				if But = 0 then
					NextState <= 1;
				End if;

			When 1 =>
				if But'event then
					DelayToBlow <= But * 1sec;
					NextState   <= 2;
				End if;

			When 2 =>
				if But = 1 then
					NextState <= 3;
				End if;
			When 3 =>
				if isInCall then

					-- Compares the incoming call's Phone number.
					-- If it is the Same phone number' the next state is initiated to the next state.
					-- It's Blow Time.
					if IncomingNum = BlowPhoneNum then
						NextState <= 4;
					End if;

				End if;
			When 4 =>
				BlowBomb <= '1' after DelayToBlow;

			When others => null;

		End Case;
	End Process Preparing_And_Blowing;
End Architecture Behaviuoral;