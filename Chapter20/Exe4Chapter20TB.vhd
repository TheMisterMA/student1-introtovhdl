--------------------------------------------------------------------------------
-- Projetc: BCD to Execess-3 converter Test Bench
-- File: Exe4Chapter20.vhd
--------------------------------------------------------------------------------
-- Description: This File Tests a BCD to Execess-3 converter
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity BCD_EX3_conv_TB is
End Entity BCD_EX3_conv_TB;

Architecture Test_Table17_2 of BCD_EX3_conv_TB is
	Component BCD_EX3_conv is
		port (
			X, Clk : in  std_logic;
			Z      : out std_logic
		);
	End Component BCD_EX3_conv;

	constant X_seq : std_logic_vector(0 to 39) := ("0000100001001100001010100110111000011001"); -- The input stream the converter will get
	constant Z_seq : std_logic_vector(0 to 39) := ("1100001010100110111000011001010111010011"); -- The output stream the converter shuold output.
	
	signal flag      : std_logic := '0';
	signal Clk : std_logic:= '1';
	signal X, Z : std_logic;
	

	signal Clk_period : time := 10ns;

Begin
	Tested_Conv : BCD_EX3_conv port map (X, Clk, Z);

	Clk <= not Clk after Clk_period; --The clock, the actual Period is 20 ns.

	Stimulate : Process
	Begin
		for index in 0 to 39 Loop
			X <= X_seq(index);

			wait for 5ns; -- Give time for Z to update.


			-- Checks if the output for the input is correct.
			if Z = Z_seq(index) then
				flag <= '0';
			else
				flag <= '1';
			End if;

			wait until rising_edge(Clk);
			wait for 5ns;

		End Loop;

	End Process Stimulate;
End Architecture Test_Table17_2;