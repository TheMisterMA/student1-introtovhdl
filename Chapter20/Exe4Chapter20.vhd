--------------------------------------------------------------------------------
-- Projetc: BCD to Execess-3 converter
-- File: Exe4Chapter20.vhd
--------------------------------------------------------------------------------
-- Description: This File Creates a BCD to Execess-3 converter
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      16/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity BCD_EX3_conv is
	port (
		X, Clk : in  std_logic;
		Z      : out std_logic
	);
End BCD_EX3_conv;

Architecture State_machine of BCD_EX3_conv is
	-- Build an enumerated type for the state machine
	type state_type is (State0, State1, State2, State3, State4, State5, State6);

	signal State, Nextstate : state_type:= State0;
Begin
	process(State, X) -- Combinational Circuit
	begin
		case State is
			when State0 =>
				if X = '0' then
					Z         <= '1';
					Nextstate <= State1;
				else
					Z         <= '0';
					Nextstate <= State2;
				end if;

			when State1 =>
				if X = '0' then
					Z         <= '1';
					Nextstate <= State3;
				else
					Z         <= '0';
					Nextstate <= State4;
				end if;

			when State2 =>
				if X = '0' then
					Z         <= '0';
					Nextstate <= State4;
				else
					Z         <= '1';
					Nextstate <= State4;
				end if;

			when State3 =>
				if X = '0' then
					Z         <= '0';
					Nextstate <= State5;
				else
					Z         <= '1';
					Nextstate <= State5;
				end if;

			when State4 =>
				if X = '0' then
					Z         <= '1';
					Nextstate <= State5;
				else
					Z         <= '0';
					Nextstate <= State6;
				end if;

			when State5 =>
				if X = '0' then
					Z         <= '0';
					Nextstate <= State0;
				else
					Z         <= '1';
					Nextstate <= State0;
				end if;

			when State6 =>
				Z         <= '1';
				Nextstate <= State0;
		end case;
	end process;

	process (CLK) -- State Register
	begin
		if rising_edge(Clk) then
			State <= Nextstate;
		end if;
	end process;
End Architecture State_machine;