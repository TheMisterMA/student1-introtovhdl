--------------------------------------------------------------------------------
-- Projetc: 16 Bit Complementer
-- File: Exe2Chapter20.vhd
--------------------------------------------------------------------------------
-- Description: This File Creates a 2's Complementer of 16 bit number it stores
--   			Int its Register.
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      17/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use IEEE.numeric_std.all;

Entity Completent_16bit is
	port (
		Clk, N    : in  std_logic;
		RegOut    : out std_logic_vector(15 downto 0) -- The End Result Register.
	);
End Entity Completent_16bit;

Architecture Behaviuoral of Completent_16bit is
	type state_type is (State0, State1, State2);

	signal State, nextState : state_type := State0;
	signal X,Z, Sh         : std_logic;
	signal K                : std_logic                     := '0';
	signal Reg              : std_logic_vector(15 downto 0) := "0110111000100010";

	signal count            : std_logic_vector(3 downto 0)  := "0000"; -- The 4-Bit Counter
Begin
	RegOut <= Reg;
	K      <= '1' when count = "1111" else '0';
	X      <= Reg(0);

	-- The out put values for each state.
	Complementer : Process(State,X)
	Begin
		Case State is
			when State0 =>
				if N = '0' then
					Sh <= '0';
					Z  <= '0';
				elsif X = '1' then
					Sh <= '1';
					Z  <= '1';
				else
					Sh <= '1';
					Z  <= '0';
				End if;

			when State1 =>
				Sh <= '1';
				if X = '1' then
					Z <= '1';
				else
					Z <= '0';
				End if;

			when State2 =>
				Sh <= '1';
				if X = '1' then
					Z <= '0';
				else
					Z <= '1';
				End if;
		End Case;
	End Process Complementer;

	-- The Counter to count until 15, when it is, k = 1.
	Counter_4bit : Process(Clk)
	Begin
		if rising_edge(Clk) then
			-- Combining The state changer as a reaction to the clock.
			Case State is
				when State0 =>
					if N = '0' then
						State <= State0;
					elsif X = '1' then
						State <= State2;
					else
						State <= State1;
					End if;

				when State1 =>
					if K = '1' then
						State <= State0;
					elsif X = '1' then
						State <= State2;
					else
						State <= State1;
					End if;

				when State2 =>
					if K = '1' then
						State <= State0;
					else
						State <= State2;
					End if;
			End case;

			-- Shifter, and adds to the counter.
			if Sh = '1' then
				Reg   <= Z&Reg(15 downto 1);
				count <= count+'1';
			End if;

		End if;
	End Process Counter_4bit;

End Architecture Behaviuoral;