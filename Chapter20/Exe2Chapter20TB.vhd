--------------------------------------------------------------------------------
-- Projetc: 16 Bit Complementer Test Bench
-- File: Exe2Chapter20TB.vhd
--------------------------------------------------------------------------------
-- Description: This File Tests a 2's Complementer of 16 bit number it stores
--   			Int its Register.
--
--------------------------------------------------------------------------------
--   Version:   |   Author:            |   Date:         |  Change:
--         2.0  |     Michael Abdalov  |      17/4/2020  |     Modificated
--              |                      |                 |
--              |                      |                 |
--------------------------------------------------------------------------------

Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity TB_16Bit_Completent is
End Entity TB_16Bit_Completent;

Architecture Test_Completent of TB_16Bit_Completent is
	Component Completent_16bit is
		port (
			Clk, N : in  std_logic;
			RegOut : out std_logic_vector(15 downto 0) -- The End Result Register.
		);
	End Component Completent_16bit;

	signal Clk        : std_logic := '0';
	signal N          : std_logic;
	signal RegOut     : std_logic_vector(15 downto 0);
	signal Clk_period : time := 10ns;
	signal sh,Z,X : std_logic;

Begin
	Clk <= not Clk after Clk_period;

	Tested_Completent : Completent_16bit
		port map (
			Clk, N, RegOut
		);

	Stimulate : Process
	Begin
		N <= '1';
		wait for 20ns;

		N <= '0';
		wait for 1000ns;
	End Process Stimulate;

End Architecture Test_Completent;